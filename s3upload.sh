BUCKET="aws-davidiog-bitbucket-syd"
ENV="dev"
BUILD_ID="${BITBUCKET_BUILD_NUMBER}"
CONTENT_TYPE="application/zip"
REGION="ap-southeast-2"
DATE=`date -R`
IN_FILE="revision.zip"
KEY="${ENV}-revision.zip"
#RESOURCE="/${BUCKET}/${KEY}"
HMAC="PUT\n\n${CONTENT_TYPE}\n${DATE}\n${RESOURCE}"

#Adding nested stacks separately. This is only required for Nested stacks
sed -i "s/@BUILD_ID/$BITBUCKET_BUILD_NUMBER/g" ${ENV}-params.json
aws s3 sync . s3://${BUCKET}/${ENV}/${BUILD_ID}/

#Zipping contents
zip -r --exclude=*.git* "$IN_FILE" .

#Uploading zip file to trigger codepipeline
aws s3 cp ${IN_FILE} s3://${BUCKET}/${ENV}/${KEY} --region ${REGION}
